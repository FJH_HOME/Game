package cn.fjh.www.quickHit;

import java.util.Scanner;

import IO.FileMethods;

import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Player implements java.io.Serializable{
	
	public static void data()
	{
		Date date=new Date();
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("当前时间为："+format.format(date));
		Calendar t=Calendar.getInstance();
		System.out.println("今天是"+t.get(Calendar.YEAR)+"年"+(t.get(Calendar.MONTH)+1)+"月"
				+t.get(Calendar.DAY_OF_MONTH)+"日");
		System.out.println("今天是星期"+(t.get(Calendar.DAY_OF_WEEK)-1));
		
	}
	
	public static void time1s()
	{
		long start=System.currentTimeMillis();
		long end;
		do
		{
			end=System.currentTimeMillis();
		}while(((int)(end-start)/1000)<1);
	}
	
	public static void start5s()
	{
		System.out.println("Are you ready？");
		for(int i=0;i<5;i++)
		{
			time1s();
			System.out.println(5-i);
		}
		System.out.println("GO!!!\n");
	}
	
	public static int out()
	{
		
		
		int levelNo=0;
		ObjectInputStream ois=null;
		try
		{
			File f=new File("C:\\Users\\mac\\Desktop\\1.txt");
			if(!f.exists())
			{
				f.createNewFile();
			}
			ois=new ObjectInputStream(new FileInputStream("C:\\Users\\mac\\Desktop\\1.txt"));
			try {
				levelNo=(int)ois.readObject();
				System.out.println("数据载入成功！\n");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}catch(IOException e)
		{
			System.out.println("你是不是傻，你都没玩过没有数据啊，算了，帮你重置等级吧！！！");
			levelNo=0;
		}finally
		{
			if(ois!=null)
			{
				try
				{
					ois.close();
				}catch(IOException e)
				{
					e.printStackTrace();
				}
			}
			
		}
		return levelNo;
	}
 
	

	private int levelNo;
	private int curScore;
	private long startTime;
	private int elapsedTime;
	
	public Player()
	{
		
	}
	
	public Player(int levelNo,int curScore,long startTime,int elapsedTime)
	{
		this.levelNo=levelNo;
		this.curScore=curScore;
		this.startTime=startTime;
		this.elapsedTime=elapsedTime;
		
	}
	
	public int getLevelNo() {
		return levelNo;
	}
	public void setLevelNo(int levelNo) {
		this.levelNo = levelNo;
	}
	public int getCurScore() {
		return curScore;
	}
	public void setCurScore(int curScore) {
		this.curScore = curScore;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public int getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(int elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	public void play()
	{
		System.out.println("*****欢迎您试玩55打字王游戏*****\n");
		data();
		Scanner input=new Scanner(System.in);
		Game game=new Game(this);
		System.out.println("\n继续上次的游戏等级吗？(y/n)");
		char ch=input.next().charAt(0);
		if(ch=='y'||ch=='Y')
		{
			this.levelNo=out();
			if(this.levelNo==6)
			{
				System.out.println("大佬，上次你都通关了！想玩是吧！给你重置等级1吧");
				this.levelNo=0;
			}
		}
		else
		{
		this.levelNo=0;
		}
		
		
		for(int i=0;i<LevelParam.levels.length;i++)
		{
			
			this.levelNo+=1;
			if(this.levelNo>1)
			{
				System.out.println("恭喜升级！！当前等级："+this.levelNo+"级\n");
				
			}
			this.curScore=0;
			this.startTime=System.currentTimeMillis();
			start5s();
			for(int j=0;j<LevelParam.levels[this.levelNo-1].getStrTimes();j++)
			{
				String outStr=game.printStr();
				String inStr=input.next();
				game.printResult(outStr, inStr);
			}
			
			
		}
	}


}
