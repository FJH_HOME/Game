package cn.fjh.www.quickHit;

public class Level {
	
	private int levelNo;
	private int strLength;
	private int strTimes;
	private int timeLimit;
	private int perScore;
	
	public Level(int levelNo,int strLength,int strTimes,int timeLimit,int perScore)
	{
		this.levelNo=levelNo;
		this.strLength=strLength;
		this.strTimes=strTimes;
		this.timeLimit=timeLimit;
		this.perScore=perScore;
	}
	public int getLevelNo()
	{
		return levelNo;
	}
	public int getStrLength()
	{
		return strLength;
	}
	public int getStrTimes()
	{
		return strTimes;
	}
	public int getTimeLimit()
	{
		return timeLimit;
	}
	public int getPerScore()
	{
		return perScore;
	}

}
