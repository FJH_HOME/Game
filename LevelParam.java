package cn.fjh.www.quickHit;

public class LevelParam {
	
	public final static Level levels[]=new Level[6];
	static 
	{
		levels[0]=new Level(1,2,6,30,1);
		levels[1]=new Level(2,3,5,26,2);
		levels[2]=new Level(3,4,4,22,5);
		levels[3]=new Level(4,5,3,18,8);
		levels[4]=new Level(5,6,2,10,10);
		levels[5]=new Level(6,7,2,10,15);
	}

}
