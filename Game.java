package cn.fjh.www.quickHit;

import java.util.Random;
import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Game implements java.io.Serializable{
	
	Player player;
	
	public Game(Player player)
	{
		this.player=player;
	}
	
	public String printStr()
	{
		int strLength=LevelParam.levels[player.getLevelNo()-1].getStrLength();
		StringBuffer buffer=new StringBuffer();
		Random random=new Random();
		
		for(int i=0;i<strLength;i++)
		{
			int rand=random.nextInt(strLength);
			switch(rand)
			{
			case 0:
				buffer.append("w");
				break;
			case 1:
				buffer.append("a");
				break;
			case 2:
				buffer.append("s");
				break;
			case 3:
				buffer.append("d");
				break;
			case 4:
				buffer.append("q");
				break;
			case 5:
				buffer.append("e");
				break;
			case 6:
				buffer.append("r");
				break;
			}	
		}
		System.out.println(buffer);
		return buffer.toString();
	}
	
	public void printResult(String out,String in)
	{
		boolean flag;
		if(out.equals(in))
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		
		if(flag==true)
		{
			long endTime=System.currentTimeMillis();
			if((endTime-player.getStartTime())/1000>LevelParam.levels[player.getLevelNo()-1].getTimeLimit())
			{
				System.out.println("您输入太慢了，已经超时，退出！");
				System.exit(1);
			}
			else
			{
				player.setCurScore(player.getCurScore()+LevelParam.levels[player.getLevelNo()-1].getPerScore());
				player.setElapsedTime((int)((endTime-player.getStartTime())/1000));
				System.out.println("输入正确，您的级别"+player.getLevelNo()+",您的积分"+player.getCurScore()
				+",已用时间"+player.getElapsedTime()+"秒");
				
				ObjectOutputStream oos=null;
				try
				{
					oos=new ObjectOutputStream(new FileOutputStream("C:\\Users\\mac\\Desktop\\1.txt"));
					oos.writeObject(player.getLevelNo()-1);   
					System.out.println("数据储存成功！\n");
				}catch(IOException e)
				{
					e.printStackTrace();
				}finally
				{
					if(oos!=null)
					{
						try
						{
							oos.close();
						}catch(IOException e)
						{
							e.printStackTrace();
						}
					}
				}
				
				int score=0;
				if(player.getLevelNo()==6)
				{
					score=LevelParam.levels[LevelParam.levels.length-1].getPerScore()*
							LevelParam.levels[LevelParam.levels.length-1].getStrTimes();
				}
				if(player.getCurScore()==score)
				{
					System.out.println("恭喜你，已经成为绝世高手！！！");
					System.exit(1);
				}
			}
		}
		else
		{
			System.out.println("输入错误，退出！！");
			System.exit(1);
		}
		
	}

}
